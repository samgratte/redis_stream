#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
# --- gene_sinusoids.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
Generate sinusoids : sinx, cosx, (sinx+cosx) toward Redis Stream
"""

import sys
from math import sin, cos, radians
from itertools import cycle
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from SimpleSHM import RedisStream, Waiting, shm_parser


parser = ArgumentParser(parents=[shm_parser], formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__)
parser.add_argument('-f', '--frequency', type=float, default=1,
                    help='frequency to send the false signals')
parser.add_argument('-a', '--add_data', action='store_true',
                    help='Init data before sending')


def main(frequency=1, add_data=False, inputs=[],
         outputs=['TEST__Data_sinusoids'], **shm_args):

    print(shm_args)
    redis = RedisStream(**shm_args)

    angles = list(map(radians, range(0, 359)))
    sinx = cycle(map(sin, angles))
    cosx = cycle(map(cos, angles))

    # DATA must be declared/initialized before !
    # redis.add_data("TEST__Data_sinusoids", sinx=0, cosx=0, sum=0)
    # or like this :
    # SHM> ADD_DATA TEST__Data_sinusoids sinx 0 cosx 0 sum 0
    if add_data:
        redis.add_data(outputs[0], 'sinx', 0, 'cosx', 0, 'sum', 0,
                       'origin', 'INIT')
    periode = Waiting(frequency=frequency)
    for s, c in zip(sinx, cosx):
        redis.write_data(outputs[0], sinx=s, cosx=c, sum=s+c)
        periode.wait_next()


if __name__ == '__main__':
    args = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}
    main(**args)
