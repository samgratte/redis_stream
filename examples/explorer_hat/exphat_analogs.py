#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- exphat_analogs.py ---
# Author  : samuel
# License : GPLv2
"""
Skeletal application
"""

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from redis_stream import RedisStream, shm_parser, Waiting


parser = ArgumentParser(parents=[shm_parser],
                        formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__, epilog='undefined')
parser.add_argument('--output', type=str, help=' ',metavar='str', default='EXPLORERHAT__Analogs')

def main(output='EXPLORERHAT__Analogs', inputs=[''], outputs=[''], **shm_args):

    redis = RedisStream(**shm_args)

    ## To SHM
    periode = Waiting(1)
    for measure in gene_measure:
        redis.write_data(outputs[0], field1 = measure.field1)
        periode.wait_next()

    ## From SHM
    out_device = Device()
    ### Cyclic work
    periode = Waiting()
    while True:
        shm_datas = redis.read_datas(*inputs)
        # do work
        out_device.write()
        periode.wait_next()

    ### Synced work
    for shm_datas in redis.watch_datas(*outputs):
        # do work
        out_device.write()


if __name__ == '__main__':
    args = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}
    main(**args)
