#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- hdl_tinyIMU.py ---
# Author  : samuel
# License : GPLv2
"""
Skeletal application
"""

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from divers import get_git_branch
from shareddata import SharedMemory, shm_parser


parser = ArgumentParser(parents=[shm_parser],
                        formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__, epilog=get_git_branch())
parser.add_argument('--i2c_bus', type=int, help=' ',metavar='int', default=1)
parser.add_argument('--gyros_address', type=str, help=' ',metavar='str', default='0x68')
parser.add_argument('--compass_address', type=str, help=' ',metavar='str', default='0x1e')
parser.add_argument('--frequency', type=float, help=' ',metavar='float', default=10.0)

def main(i2c_bus=1, gyros_address='0x68', compass_address='0x1e', frequency=10.0, inputs=[''], outputs=['TinyIMU__PitchRollYaw', 'TinyIMU__Speed_vector'], **shm_args):

    with SharedMemory(**shm_args) as shm:

        def print_trame(trame):
            print('%s received %s : %s' % (shm_args['app_name'],
                                           trame.name, trame))
        shm_read = shm.read if 'frequency' in vars() else shm.watch
        ### only inputs ###
        if len(inputs) > 1:
            for trames in shm_read(inputs):
                for trame in trames:
                    print_trame(trame)
        elif len(inputs) == 1:
            for trame in shm_read(inputs[0]):
                print_trame(trame)
        else:
            ### only outputs ###
            shm_data = shm.get_data_hdlr(outputs[0], validity=10, priority=99)
            while True:
                # reading sensor for ex
                shm_data.write(field1=result1, field2=result2)

        ### inputs and outputs ##

        shm_data = shm.get_data_hdlr(outputs[0], validity=10, priority=99)
        for trame1, trame2 in shm_read(inputs):
            # trame1 & trame2 refreshed from shm at every loop
            # doing some work ...
            shm_data.write(field1=result1, field2=result2)
            # wait for next iteration ... (with divers.Waiting() for ex.)

if __name__ == '__main__':
    main(**vars(parser.parse_args()))
