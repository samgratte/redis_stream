#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- simu_motor.py ---
# Author  : samuel
# License : GPLv2
"""
Skeletal application
"""

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from random import random

from redis_stream import RedisStream, shm_parser


parser = ArgumentParser(parents=[shm_parser],
                        formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__, epilog='undefined')


def main(inputs=['EXPHAT__Motors_cmd'], outputs=['EXPHAT__Motors_status'],
         **shm_args):

    redis = RedisStream(**shm_args)

    for motors_cmd in redis.watch_datas(inputs[O]):
        outdict = {k: v + (random() * 6) - 3
                   for k, v in motors_cmd._asdict().items()}
        redis.write_data(outputs[O], **outdict)


if __name__ == '__main__':
    args = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}
    main(**args)
