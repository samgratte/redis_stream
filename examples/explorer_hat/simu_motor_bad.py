#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- simu_motor.py ---
# Author  : samuel
# License : GPLv2
"""
Skeletal application
"""

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from redis_stream import RedisStream, shm_parser


parser = ArgumentParser(parents=[shm_parser],
                        formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__, epilog='undefined')
parser.add_argument('--input', type=str, help=' ',metavar='str', default='EXPHAT__Motors_cmd')
parser.add_argument('--output', type=str, help=' ',metavar='str', default='EXPHAT__Motors_status')

def main(input='EXPHAT__Motors_cmd', output='EXPHAT__Motors_status', inputs=[''], outputs=[''], **shm_args):

    redis = RedisStream(**shm_args)

    ### Synced work
    for motors_cmd in redis.watch_datas('EXPHAT__Motors_cmd'):
        # do work
        motor1_status = (random.random() * 6) - 3 + motors_cmd.motor1
        motor2_status = (random.random() * 6) - 3 + motors_cmd.motor2

        redis.write_data('EXPHAT__Motors_status', motor1=motor1_status,
                         motor2=motor2_status)


if __name__ == '__main__':
    args = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}
    main(**args)
