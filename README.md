---
title: **SimpleSHM** a shared memory based on _Redis_ streams
# author: Samuel BUCQUET
date:
dedication:
resume:
documentclass: scrartcl
classoption:
# - twoside
# - chapterprefix
# - headings=big
# - appendixprefix
# - chapterentrydots
# - sectionentrydots
 - headsepline
 - footsepline
 - openany
 - oneside
 #- landscape
 #- twocolumn
fontfamily: libertine
fontsize: 11pt
lang: fr
papersize: a3
geometry:
 - vmargin=30mm
 - hmargin=20mm
numbersections: false
links-as-notes: true
# toc: true
---
# Introduction


`redis_stream.py` is a cli program, a python library and a stdin/stdout interface to redis stream.

- The first use case, cli program, involve sending a command to its stdin.
- The second, as a python library, offers a RedisStream object with its API and a parser to handle command line arguments in a homogeneous way.
- The third use allows to plug stdin and stdout to anything in order to use its functions.
The main use is to wrap it around `websocketd` to allow web client to use this redis stream interface.

# Data format

Data are stored by Redis in a central memory as a stream of timestamped logs.
Each log records the data, comprised of fields and values and its timestamp.
There is mandatory fields for a data log :

- *name*: a unique name (the stream key in Redis)
- *origin*: who emitted the data
- *validity*: how much time is valid a data since it was emitted
- *priority*: if a data is emitted from multiple senders, the highest priority wins.  
  **TODO**: verify how this will work for a client
- *maxlen*: the maximum number of items for a data kept in memory and retrievable
- *logstep*: each emitter is responsible to write in a logfile its data according to logstep.  
           0 disables it, 1 for recording each, 2 for one in two, ...

then a data must comprise at least one (field,value) pair.

## Data Description

There is two levels of data access. The first one requires that each client receiving or emitting a data knows the data format precisely, this is for low level application parts like control/command.

The other level can post new data without a priori knowledge about it, like GUI.

For the application requiring data format knowledge, each emitter writes a `data_desc.ini` merged with the others when the global application launchs.

The file syntax follows the `ini` config file syntax like this :
    
    ### datadesc_example.ini ###
    [COMMON]
    maxlen = 10
    origin = INIT
    validity = 0
    priority = 0
    logstep = 0  ; '0' disable log, '1' log each write, '2' log only
                  one in two, ...
    [TEST__Data_name1]
    value1 = 0.0  ; float
    value2 = 0    ; integer
    value3 = '3'  ; string

    [TEST__Data_name2]
    value = 0
    logstep = 1  ; override default value

Each file must contains a `[COMMON]` section with default values,
then each data described is inside a [*Dataname*] section with
fieldnames as key and initial values as values with type inferred from.

**TODO**: add `data_types.ini` for instantianting data description from

At the beginning of the application execution, we init the memory with the datadesc files like this :
```bash
$ echo "INIT_DB $(find ./ -name datadesc_*.ini)"|./redis_stream.py -X INIT
```

# CLI usage

```bash
redis_stream.py [-h] [-H REDIS_HOST] [-P REDIS_PORT] [-t TIMEOUT]
                   [-x SENDER_NAME] [-j] [-d]
optional arguments:
  -h, --help            show this help message and exit
  -j, --json_output     ask for json output formatting
  -d, --debug           enable debugging commands input

REDIS:
  -H REDIS_HOST, --redis_host REDIS_HOST
                        CPU hosting redis server (localhost)
  -P REDIS_PORT, --redis_port REDIS_PORT
                        tcp port number to connect to redis server (6379)
  -T TIMEOUT, --timeout TIMEOUT
                        timeout for connectiong to redis server
  -X SENDER_NAME, --sender_name SENDER_NAME
                        Tell the name of the offender (redis_stream.py)
  -D, --shm_debug       enable debugging commands and answers with redis
  -I INIT_MSG, --init_msg INIT_MSG
                        Env variable name to signal application is ready to start
  --inputs [DATANAME [DATANAME ...]]
                        reading data list
  --outputs [DATANAME [DATANAME ...]]
                        writing data list
```

## Actions parsed on stdin

### ADD_DATA
*ADD_DATA dataname [maxlen _max_] key1 value1 [key2 value2 ...]*

Adds the data *dataname* without checking first if it exists.
If the data existed before, the previous fields won't be present if not supplied.
Litteral values must be quoted with single quote `'` :  

`SHM> ADD_DATA mydata maxlen 0 myfloat 3.0 mylitteral '3.0' mytwowords 'one two'`  
*maxlen* defines the maximum number of records to keep in memory
and if negative, asks for a strict compliance of the maximum.
if null, there is no limit !
The default is one million records, which holds more than a day @10Hz.

### WRITE_DATA
*WRITE_DATA dataname key1 value 1 [key2 value2 ...]*

Writes new values for data with data already present.
Litteral values must be quoted with single quote "'" :  

`SHM> ADD_DATA mydata myfloat 3.0 mylitteral '3.0' mytwowords 'one two'`

### WRITE_DATAS
*WRITE_DATAS dataname1 key1=value1 [key2=value2 ...] [dataname2 key3=value3 [key4=value4 ...]]*

Writes simultaneously multiple data with their respective values.

### READ_DATA(S)
*READ_DATA(S) dataname1 [dataname2 ...]*

Returns immediatly the last occurence of data(s) either as a json dict or a namedtuple.
Even for a single data read, a tuple of data is returned.

### DATA_RANGE
*DATA_RANGE dataname first_ts last_ts count reverse*

Returns for data the range of values recorded between last and
first timestamp ('-1' is the last timestamp recorded, '0' the first)  
*count*: number of occurence to return (0 for all)  
*reverse*: 1 will retrieve from the last record 0 from the first

### WATCH_DATA(S)
*WATCH_DATA(S) frequency dataname [dataname ...]*

Returns a data(s) generator, which will yield a tuple of data at frequency as their last known values.

### WAIT_DATA(S)
*WAIT_DATA(S) timeout dataname [dataname ...]*

Returns a data(s) generator, which will block this client.
Each iteration yield a tuple of data freshly written with a timeout before giving up.

### REMOVE_DATA(S)
*REMOVE_DATA(S) dataname1 [dataname2 ...]*

Removes all entries pertaining to dataname(s).  
/!\\ BEWARE /!\\ it is definitive !

### LIST_DATA
*LIST_DATA *pattern*

Returns a list of datanames matching the pattern


### DATA_TREE
*DATA_TREE _pattern_*

Returns a tree for datanames matching the pattern

### ADD_ALIAS 
*ADD_ALIAS dataname alias*

Affects an alias to a dataname

### INIT_DB 
*INIT_DB filename [filename ...]*

Initializes **redis** with description of datas in one or multiple `*.ini` files.  
See [Data Description](#data-description) above

### JSON
*JSON* Toggles json output formatting

## DEBUG
*DEBUG* Toggles debug mode

### HELP
*HELP* print the this help.

### Quit
*CTRL-D* to end the session.

# Wrapping tools

It is meant to be wrapped by `rlwrap` (<https://github.com/hanslub42/rlwrap>)
for usage in command line mode :

```bash
$ rlwrap -r -f . -A -p -S 'SHM> ' ./redis_stream.py -P 9999 -j -d -x 'CLI'
```

Or it can also be wrapped by `websocketd` (<https://github.com/joewalnes/websocketd>)
for serving the cli interface by websocket (hence the json output format) :
```bash
$ websocketd --staticdir brythonapp --port=8989 --loglevel=debug ./redis_stream.py -P 9999 -j -x 'WS'
```
# Tutorials

## bash

### Init a data

```bash
echo "ADD_DATA TEST__Data_random maxlen 0 value 0" | ./redis_stream.py -x 'INIT'
```

### Generate a random value

```bash
while true
do
  echo "WRITE_DATA TEST__Data_random value $RANDOM"
  sleep 0.2
done | ./redis_stream.py -x "loop"
```

## python

Here is a data generator in Python.
The lines 4, 8, (15) & 20 are using the redis stream interface.

```python
#!/usr/bin/python3.6 -u
# -*- coding: utf-8 -*-
# --- redis_stream.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
Generate sinusoids : sinx, cosx, (sinx+cosx) toward Redis Stream
"""

import sys
from math import sin, cos, radians
from itertools import cycle
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from redis_stream import RedisStream, Waiting, shm_parser


parser = ArgumentParser(parents=[shm_parser], formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__)
parser.add_argument('-f', '--frequency', type=float, default=1,
                    help='frequency to send the false signals')
parser.add_argument('-a', '--add_data', action='store_true',
                    help='Init data before sending')


def main(frequency=1, add_data=False, inputs=[],
         outputs=['TEST__Data_sinusoids'], **shm_args):

    print(shm_args)
    redis = RedisStream(**shm_args)

    angles = list(map(radians, range(0, 359)))
    sinx = cycle(map(sin, angles))
    cosx = cycle(map(cos, angles))

    # DATA must be declared/initialized before !
    # redis.add_data("TEST__Data_sinusoids", sinx=0, cosx=0, sum=0)
    # or like this :
    # SHM> ADD_DATA TEST__Data_sinusoids sinx 0 cosx 0 sum 0
    if add_data:
        redis.add_data(outputs[0], 'sinx', 0, 'cosx', 0, 'sum', 0,
                       'origin', 'INIT')
    periode = Waiting(frequency=frequency)
    for s, c in zip(sinx, cosx):
        redis.write_data(outputs[0], sinx=s, cosx=c, sum=s+c)
        periode.wait_next()


if __name__ == '__main__':
    args = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}
    main(**args)
```

## javascript

Now, a web client connected to `websocketd` can receive (and send) datas :

```html
<!DOCTYPE html>
<html>
  <head>
    <script type="text/javascript" src="smoothie.js"></script>
    <script type="text/javascript">
    var ws = new WebSocket("ws://localhost:8989");
    var datas = new TimeSeries();

    ws.onopen = function()
    {
      /*setInterval(function() {
        ws.send("READ_DATA TEST__Data_random");
      }, 2000);*/
      ws.send("WATCH_DATA 0 TEST__Data_random")
    };

    ws.onmessage = function (evt)
    {
      var data = JSON.parse(evt.data)["TEST__Data_random"][0];
      datas.append(data.timestamp*1000, data.value);
    };

    ws.onclose = function()
    {
      // websocket is closed.
      console.log("Connection is closed...");
    };

    window.onbeforeunload = function(event) {
      ws.close();
    };

    function createTimeline() {
      var chart = new SmoothieChart({interpolation:'line',timestampFormatter:SmoothieChart.timeFormatter});
      chart.addTimeSeries(datas, { strokeStyle: 'rgba(0, 255, 0, 1)',
        fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
      chart.streamTo(document.getElementById("chart"), 500);
    };
    </script>
  </head>
  <body onload="createTimeline()">
    <canvas id="chart" width="600" height="200"></canvas>
  </body>
</html>

```
