# -*- coding: utf-8 -*-
# --- SimpleSHM/utils.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
"""


from time import time, sleep
from ast import literal_eval


class Waiting:

    def __init__(self, frequency=None, periode=None, set_start=True):
        if frequency is None and periode is None:
            raise ValueError
        self.periode = 1.0/frequency if frequency else periode
        if set_start:
            self.set_start()

    def set_start(self):
        self.start_time = time()

    def wait_next(self, set_start=True):
        time_passed = time() - self.start_time
        if time_passed < self.periode:
            sleep(self.periode - time_passed)
        if set_start:
            self.set_start()


def parse_strval(v):
    try:
        return literal_eval(v)
    except (SyntaxError, TypeError, ValueError):
        return v


def parse_values(values):
    for v in values:
        v = v.strip()
        try:
            if len(v) == 0:
                return v
            # Search for a literal string between '@'
            elif v[0] == '@' and v[-1] == '@':
                yield v[1:-1]
            # Search for an integer between '#'
            elif v[0] == '#' and v[-1] == '#':
                yield int(v[1:-1])
            # Search for a float between '^'
            elif v[0] == '^' and v[-1] == '^':
                yield float(v[1:-1])
            # Search for a boolean string value
            elif v.upper() in ('TRUE', 'YES', 'ON'):
                yield True
            elif v.upper() in ('FALSE', 'NO', 'OFF'):
                yield False
            else:
                yield v
        except (TypeError, ValueError):
            yield v


def format_values(values):
    for v in values:
        if type(v) == int:
            yield '#' + repr(v) + '#'
        elif type(v) == float:
            yield'^' + repr(v) + '^'
        elif type(v) == bool:
            yield repr(v)
        else:
            yield repr(v).replace("'", "@")
