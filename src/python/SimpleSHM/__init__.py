# -*- coding: utf-8 -*-
# --- SimpleSHM/__init__.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
"""


from .redis_stream import RedisStream, shm_parser, RedisHelpFormatter
from .utils import Waiting
