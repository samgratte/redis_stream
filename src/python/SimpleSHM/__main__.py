# -*- coding: utf-8 -*-
# --- SimpleSHM/__main__.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
"""


from .redis_stream import parser, main

args = parser.parse_args()
main(**vars(args))
