#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
# --- redis_stream.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
"""

import os
import sys
import shutil
import shlex
import psutil
import platform
from time import sleep, time
from socket import create_connection, timeout as SocketTimeout
from collections import namedtuple, defaultdict
# Python < 3.6
from collections import OrderedDict
from argparse import ArgumentParser, RawDescriptionHelpFormatter, ArgumentDefaultsHelpFormatter
import json
from datetime import datetime, timezone
import csv
from subprocess import check_output
from configparser import ConfigParser, ExtendedInterpolation
from pprint import pprint
from hiredis import Reader

from .utils import Waiting, parse_strval, parse_values, format_values


class RedisHelpFormatter(ArgumentDefaultsHelpFormatter, RawDescriptionHelpFormatter):
    pass


epilog = """
It is meant to be wrapped by 'rlwrap' (https://github.com/hanslub42/rlwrap)
for usage in command line mode :
$ rlwrap -r -f . -A -p -S 'SHM> ' ./redis_stream.py -P 9999 -j -d -D -X 'CLI'

Or it can also be wrapped by 'websocketd' (https://github.com/joewalnes/websocketd)
for serving the cli interface by websocket (hence the json output format) :
$ websocketd --staticdir brythonapp --port=8989 --loglevel=debug ./redis_stream.py -P 9999 -j -X 'WS'

"""

# allows the output to take in the real terminal size
os.environ['COLUMNS'] = str(shutil.get_terminal_size().columns)

# FIXME: Test if the git arborescence exists and grab the actual branch of the
# module executed
# __app_version = check_output('git describe', shell=True).decode('utf-8') \
#     if os.path.exists('.git/HEAD') else 'undefined'
__app_version = 'v0.8'


class DataNotInMemory(Exception):
    """Data not in memory"""


class DataBadFormat(Exception):
    """Bad formatted Data in memory"""


class RedisStream:

    def __init__(self, redis_host='127.0.0.1', redis_port=6379, timeout=None,
                 sender_name=sys.argv[0].strip('./'), init_msg="", shm_debug=False):
        if timeout is None:
            timeout = 60
        self.emitter = platform.node() + ':' + sender_name
        self.cnx = create_connection((redis_host, redis_port),
                                     timeout=timeout)
        self.reader = Reader(encoding='utf-8')
        self.init_msg = init_msg
        self.debug = shm_debug
        self.aliases = dict()
        if self.init_msg:
            self.wait_for_init()

    def wait_for_init(self):
        for msg in self.wait_datas(self.init_msg):
            print('Waiting for init msg ({}) ...'.format(self.init_msg), end="")
            if msg['name'] == self.init_msg:
                print("OK")
                return
            print("NOK")

    def _execute(self, cmd):
        cmd += '\r\n'
        if self.debug:
            print('cmd:> {}'.format(cmd.strip()), file=sys.stderr)
        self.cnx.sendall(cmd.encode('utf-8'))
        while True:
            try:
                buff = self.cnx.recv(4096)
            except SocketTimeout:
                print('Redis timed out !', file=sys.stderr)
                continue
            self.reader.feed(buff)
            result = self.reader.gets()
            if result is not False:
                break
            sleep(0.1)
        if self.debug:
            print('answer:> {}'.format(result), file=sys.stderr)
        return result

    def _parse_stream_data(self, *datas):
        for datalist in datas:
            if not datalist:
                continue
            for data in datalist:
                if not data:
                    continue
                try:
                    timestamp, fieldvalues = data
                    timestamp = float(timestamp.split('-')[0])/1000
                    fields = ['timestamp'] + fieldvalues[::2]
                    values = [timestamp] + list(parse_values(fieldvalues[1::2]))
                except (ValueError, TypeError):
                    raise DataBadFormat(repr(data))
                ddict = OrderedDict((zip(fields, values)))
                if type(ddict['validity']) in {int, float}:
                    if (time() - timestamp) > ddict['validity']:
                        ddict['validity'] = False
                yield ddict
                """
                The right return data type would be namedtuple
                but the creation time of a namedtuple is a bottleneck
                see (https://lwn.net/Articles/731423/)
                so we will have to stay on OrderedDict for now
                ...
                yield namedtuple(dataname, fields)._make(*values)
                ...
                or we can also cache the named tuples in self._nts
                ...
                if dataname not in self._nts:
                    self._nts[dataname] = namedtuple(dataname, fields)
                yield self._nts[dataname]._make(*values)
                ...
                """

    def get_data_range(self, dataname, begin=0, end=-1, count=None,
                       reverse=True, in_multi=False):
        dataname = self.aliases.get(dataname, dataname)
        count = 'COUNT {}'.format(count) if count else ''
        begin = '-' if begin == 0 else '{}-0'.format(int(begin*1000))
        end = '+' if end == -1 else '{}-0'.format(int(end*1000))
        if reverse:
            cmd = 'XREVRANGE {} {} {} {}'.format(dataname, end, begin, count)
        else:
            cmd = 'XRANGE {} {} {} {}'.format(dataname, begin, end, count)
        result = self._execute(cmd)
        if result == []:
            raise DataNotInMemory(str(dataname))
        if reverse:
            result = result[::-1]
        return result if in_multi else self._parse_stream_data(result)

    def read_data(self, dataname):
        return list(self.get_data_range(dataname, count=1))[0]

    def read_datas(self, *datanames):
        self._execute('MULTI')
        for dataname in datanames:
            self.get_data_range(dataname, count=1, in_multi=True)
        result = self._execute('EXEC')
        return self._parse_stream_data(*result)

    def watch_datas(self, *datanames, frequency=1):
        # keep serving the last data written at frequency
        periode = Waiting(frequency=frequency)
        while True:
            yield tuple(self.read_datas(*datanames))
            periode.wait_next()

    def wait_datas(self, *datanames, timeout=0):
        datanames = [self.aliases.get(name, name) for name in datanames]
        # block until data is available
        cmd = 'XREAD BLOCK {} COUNT 1 STREAMS {}{}'.format(
            int(timeout*1000), ' '.join(datanames), ' $'*len(datanames))
        while True:
            result = self._execute(cmd)
            if result is None:
                print('Watch Timeout !', file=sys.stderr)
                return
            dataname, data = result[-1]
            yield list(self._parse_stream_data(data))[0]

    def _xadd_data(self, dataname, kvargs):
        dataname = self.aliases.get(dataname, dataname)
        # handle CAPPED streams
        strict_maxlen = kvargs['maxlen'] < 0
        maxlen = abs(kvargs['maxlen'])
        maxlen = 'MAXLEN {} {}'.format('' if strict_maxlen else '~', maxlen) if maxlen else ''
        # format STREAM-ID from timestamp
        if kvargs['timestamp'] == '*':
            ts = '*'
            kvargs['timestamp'] = time()
        else:
            ts = str(int(float(kvargs['timestamp']) * 1000)) + '-0'
        self.log_data(kvargs)
        del kvargs['timestamp']
        cmd = 'XADD {} {} {} {}'.format(
            dataname, maxlen, ts,
            ' '.join(k+' '+v for k, v in zip(kvargs, format_values(kvargs.values()))))
        self._execute(cmd)

    def add_data(self, dataname, *args):
        """
          `maxlen` defines the maximum number of record to keep in memory
          and if negative, asks for a strict compliance of the maximum.
          if null, there is no limit !
          The default is one million records, which holds more than a day @10Hz.
        """
        # in Python 3.6 we would be able to parse **kvargs now we have to
        # use *args to keep order of arguments
        it = iter(args)
        kvargs = OrderedDict(((k, v) for k, v in zip(it, it)))
        ddict = OrderedDict((('timestamp', '*'), ('name', dataname),
                             ('origin', self.emitter if self.emitter else 'Init'),
                             ('priority', 0), ('validity', False), ('maxlen', 1000)))
        ddict.update(kvargs)
        self._xadd_data(dataname, ddict)

    def write_data(self, dataname, prevdatas={}, validity=True, **kvargs):
        # we garanty the order and the relevant values
        # prevdata is an OrderedDict
        prevdata = prevdatas[dataname] if dataname in prevdatas\
            else self.read_data(dataname)
        origin = kvargs.get('origin', self.emitter)
        prevdata['timestamp'] = '*'  # default to the current time
        # arguments replaces previous datas with new metadata
        prevdata.update(kvargs, origin=origin, validity=validity)
        self._xadd_data(dataname, prevdata)

    def write_datas(self, **datapairs):
        """
        TODO:
         ^
        /!\ Broken because of the read_data inside MULTI/EXEC
        ---
        write multiple datas with MULTI/EXEC if len(datas) > 1
        """
        prevdatas = self.read_datas(return_namedtuples=False, *datapairs.keys())
        self._execute('MULTI')
        for dataname in datapairs:
            self.write_data(dataname, prevdatas=prevdatas, **datapairs[dataname])
        self._execute('EXEC')

    def remove_datas(self, *datanames):
        self._execute('MULTI')
        for dataname in datanames:
            self._execute('DEL {}'.format(dataname))
        self._execute('EXEC')

    def list_datas(self, pattern):
        result, cursor = [], 0
        while True:
            cursor, match = self._execute('SCAN {} MATCH {}'.format(cursor, pattern))
            result.extend(match)
            if cursor == "0":
                break
        return sorted(result)

    def add_alias(self, dataname, alias):
        self.aliases[dataname].add(alias)

    def log_data(self, datadict):
        logdir = os.getenv('LOGDIR')
        if not logdir:
            return
        if not hasattr(self, 'logfile'):
            self.logfile = {}
        ddict = datadict.copy()
        dataname = ddict['name']
        for key in ('name', 'origin', 'maxlen'):
            del ddict[key]
        if dataname not in self.logfile:
            timestamp = datetime.fromtimestamp(ddict['timestamp'], timezone.utc)
            timestamp = timestamp.strftime('%Y%m%d-%T')
            logfilename = '{}-{}-{}.log'.format(timestamp, self.emitter, dataname)
            logfilename = os.path.join(logdir, logfilename)
            self.logfile[dataname] = csv.DictWriter(open(logfilename, 'w', newline=''),
                                                    list(ddict.keys()), restval='UNDEFINED',
                                                    delimiter=';', quotechar="'",
                                                    quoting=csv.QUOTE_MINIMAL)
            self.logfile[dataname].writeheader()
        self.logfile[dataname].writerow(ddict)

    def write_init_msg(self):
        if self.init_msg:
            self.add_data(self.init_msg)

    def init_db(self, *filenames):
        self._execute('MULTI')
        datadesc = ConfigParser(default_section='COMMON',
                                interpolation=ExtendedInterpolation())
        datadesc.read(filenames)
        for dataname in datadesc.sections():
            items = sum(datadesc.items(dataname), ())
            self.add_data(dataname, *(parse_strval(arg) for arg in items))
        self.write_init_msg()
        self._execute('EXEC')


def get_data_tree(*datanames):
    data_tree = defaultdict(lambda: defaultdict(list))
    for name in datanames:
        lvl1, lvl2 = name.split('__')[:2]
        lvl2, *lvl3 = lvl2.split('_')
        lvl3 = '_'.join(lvl3)
        data_tree[lvl1][lvl2].append(lvl3)
    return data_tree


def datas_print(*datas, json_output=False):
    if not datas:
        return
    if json_output:
        print(json.dumps(datas))
    elif hasattr(datas[0], 'keys'):
        for data in datas:
            if hasattr(data, 'keys'):
                data['horodatage'] = datetime.utcfromtimestamp(data['timestamp'])
                data.move_to_end('name', False)
                print('\t', '\n\t'.join('{:10}: {}'.format(k, v)
                                        for k, v in data.items()), sep='')
                print('')
    elif hasattr(datas[0], 'items'):
        pprint({k: dict(v) for k, v in datas[0].items()}, indent=4, width=60)
    else:
        print('\n'.join(datas))


def main(json_output=False, debug=False, inputs=[], outputs=[], **shm_args):
    """
Redis Stream Interface

ACTION parsed on sys.stdin :

ADD_DATA dataname [maxlen _max_] key1 value1 [key2 value2 ...]
-> add the data dataname without checking first if it exists.
    if the data existed before, the previous fields won't be present
    if not supplied.
    Litteral values must be quoted with single quote "'" :
    SHM> ADD_DATA mydata myfloat 3.0 mylitteral '3.0' mytwowords 'one two'
MAXLEN defines the maximum number of records to keep in memory
and if negative, asks for a strict compliance of the maximum.
if null, there is no limit !
The default is one million records, which holds more than a day @10Hz.

WRITE_DATA dataname key1 value 1 [key2 value2 ...]
-> write new values for data with data already present.
    Litteral values must be quoted with single quote "'" :
    SHM> ADD_DATA mydata myfloat 3.0 mylitteral '3.0' mytwowords 'one two'

WRITE_DATAS dataname1 key1=value1 [key2=value2 ...] [dataname2 key3=value3 [key4=value4 ...]]
-> write simultaneously multiple data with their respective values.

READ_DATA(S) dataname1 [dataname2 ...]
-> return immediatly the last occurence of data(s) either as a json
    dict or a namedtuple.
    Even for a single data read, a tuple of data is returned.

DATA_RANGE dataname first_ts last_ts count reverse
-> return for data the range of values recorded between last and
    first timestamp ('-1' is the last timestamp recorded, '0' the first)
count: number of occurence to return (0 for all)
reverse: 1 will retrieve from the last record 0 from the first

WATCH_DATA(S) frequency dataname [dataname ...]
-> return a data(s) generator, which will yield a tuple of data at
    frequency as their last known values.

WAIT_DATA(S) timeout dataname [dataname ...]
-> return a data(s) generator, which will block this client.
    each iteration yield a tuple of data freshly written
    with a timeout before giving up.

REMOVE_DATA(S) dataname1 [dataname2 ...]
-> remove all entries pertaining to dataname(s).
/!\\ BEWARE /!\\ it is definitive !

LIST_DATA *pattern*
-> return a list of datanames matching the pattern

DATA_TREE *pattern*
-> return a tree for datanames matching the pattern

ADD_ALIAS dataname alias
-> affects an alias to a dataname

INIT_DB filename [filename ...]
-> initialise redis with description of datas in one or multiple '.ini
    files.
    Each file must contains a [COMMON] section with default values,
    then each data described is inside a [Dataname] section with
    fieldnames as key and initial values as values
    with type inferred from, like this :
        ### datadesc_example.ini ###
        [COMMON]
        maxlen = 10
        origin = INIT
        validity = 0
        priority = 0
        logstep = 0  ; '0' disable log, '1' log each write, '2' log only
                        one of two, ...
        [TEST__Data_name1]
        value1 = 0.0  ; float
        value2 = 0    ; integer
        value3 = '3'  ; string

        [TEST__Data_name2]
        value = 0
        logstep = 1  ; override default value


JSON
-> toggle json output formatting

DEBUG
-> toggle debug mode

HELP
-> this text.

CTRL-D (or QUIT or EXIT) end the session.

    """
    # 'import cmd' would have complexified the code here ...
    redis = RedisStream(**shm_args)
    syntax_error = False
    for line in sys.stdin:
        if debug:
            print('INPUT:', line, file=sys.stderr)
        if line == "\n" or line == "\r\n":
            continue
        try:
            cmd, *args = shlex.split(line)
        except ValueError:
            print('ERR: syntax error', main.__doc__)
            continue
        cmd = cmd.upper()
        try:
            if 'READ_DATA' in cmd:
                datas_print(*redis.read_datas(*args),
                            json_output=json_output)
            elif 'DATA_RANGE' in cmd:
                dataname = args[0]
                begin = float(args[1])
                end = float(args[2])
                count = int(args[3])
                reverse = bool(int(args[4]))
                datas = redis.get_data_range(dataname, begin, end,
                                             count, reverse)
                datas_print(*datas, json_output=json_output)
            elif cmd == 'WRITE_DATAS':
                datas = OrderedDict()
                for arg in args:
                    if '=' in arg:
                        key, value = arg.split('=')
                        datas[dataname][key] = value
                    else:
                        dataname = arg
                        datas[dataname] = {}
                redis.write_datas(**datas)
            elif cmd in ('WRITE_DATA', 'ADD_DATA'):
                it = iter(args)
                dataname = next(it)
                if cmd == 'ADD_DATA':
                    if len(args) < 4:
                        syntax_error = True
                        continue
                    redis.add_data(dataname, *(parse_strval(arg) for arg in it))
                else:
                    redis.write_data(dataname, **{k: parse_strval(v) for k, v in zip(it, it)})
            elif 'WATCH_DATA' in cmd:
                # keep serving data so no more input
                for datas in redis.watch_datas(*args[1:], frequency=float(args[0])):
                    datas_print(*datas, json_output=json_output)
            elif 'WAIT_DATA' in cmd:
                # keep looking for data so no more input
                print("Waiting ...", file=sys.stderr)
                for data in redis.wait_datas(*args[1:], timeout=float(args[0])):
                    datas_print(data, json_output=json_output)
                    print("Waiting ...", file=sys.stderr)
                print("Ready:")
            elif 'LIST_DATA' in cmd:
                if args == []:
                    args = ['*']
                datas_print(*redis.list_datas(*args),
                            json_output=json_output)
            elif 'DATA_TREE' in cmd:
                datas_print(get_data_tree(*redis.list_datas(*args)), json_output=json_output)
            elif 'ADD_ALIAS' in cmd:
                redis.add_alias(args[0], args[1])
            elif 'REMOVE_DATA' in cmd:
                redis.remove_datas(*args)
            elif 'INIT_DB' in cmd:
                redis.init_db(*args)
            elif 'DEBUG' in cmd:
                redis.debug = not redis.debug
            elif 'JSON' in cmd:
                json_output = not json_output
            elif 'HELP' in cmd:
                print(main.__doc__)
            elif 'QUIT' in cmd or 'EXIT' in cmd:
                break
            else:
                syntax_error = True
        except DataNotInMemory:
            print("ERR: DataNotInMemory")
        except DataBadFormat:
            print("ERR: DataBadFormat")
        except ValueError:
            syntax_error = True
        except KeyboardInterrupt:
            print("Stopping WAIT & WATCH, ready:")
        if syntax_error:
            print("ERR: wrong command or arguments!")
            print("Syntax error! type 'HELP'")
            syntax_error = False


shm_parser = ArgumentParser(add_help=False)
shm_group = shm_parser.add_argument_group('REDIS')
shm_group.add_argument('-H', '--redis_host', type=str, default='localhost',
                       help='CPU hosting redis server (%(default)s)')
shm_group.add_argument('-P', '--redis_port', type=int, default=6379,
                       help='tcp port number to connect to redis server (%(default)d)')
shm_group.add_argument('-T', '--timeout', type=float, default=None,
                       help='timeout for connectiong to redis server')
shm_group.add_argument('-X', '--sender_name', default=os.path.basename(sys.argv[0]),
                       help='Tell the name of the offender (%(default)s)')
shm_group.add_argument('-D', '--shm_debug', action='store_true',
                       help='enable debugging commands and answers with redis')
shm_group.add_argument('-I', '--init_msg', type=str, default=None,
                       help="Env variable name to signal application is ready to start")
shm_group.add_argument('--inputs', type=str, default=None, nargs='*',
                       metavar='DATANAME', help='reading data list')
shm_group.add_argument('--outputs', type=str, default=None, nargs='*',
                       metavar='DATANAME', help='writing data list')

parser = ArgumentParser(parents=[shm_parser], formatter_class=RedisHelpFormatter,
                        description=main.__doc__,
                        epilog=epilog+'\n'+'GIT: '+__app_version)
parser.add_argument('-j', '--json_output', action='store_true',
                    help='ask for json output formatting')
parser.add_argument('-d', '--debug', action='store_true',
                    help='enable debugging commands input')


if __name__ == '__main__':
    args = parser.parse_args()
    main(**vars(args))
