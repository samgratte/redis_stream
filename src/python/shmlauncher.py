#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- launcher.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
"""

import os
from argparse import ArgumentParser
from configparser import ConfigParser, ExtendedInterpolation
from itertools import chain, starmap
from concurrent.futures import ProcessPoolExecutor
from importlib import import_module
from setproctitle import setproctitle
from time import sleep
from collections import OrderedDict
from socket import gethostname


def parse_config(config_file):
    cfg = ConfigParser(default_section='COMMON',
                       interpolation=ExtendedInterpolation())
    cfg.read(config_file)
    dico_conf = OrderedDict()
    for app in cfg.sections():
        options = cfg[app]
        if options['host'] not in ('localhost', gethostname()):
            continue
        module = options['exec'][:options['exec'].index('.py')]
        meta_options = ('exec', 'host', 'hardware_in', 'hardware_out', 'args')
        options.update(app_name=app)
        args = list(chain.from_iterable(('--'+k, options[k]) for k in options
                                        if k not in meta_options))
        if 'args' in options:
            args.extend(options['args'].split('\n'))
        dico_conf[app] = module, list(args)
    return dico_conf


def call_main(app, module, args):
    mod = import_module(module)
    setproctitle(app)
    mod.parser.prog = app
    print(app, args)
    args = vars(mod.parser.parse_args(args))
    mod.main(**args)


def main(config_file, max_workers=None):
    """
    Analyse le fichier de configuration passé en argument
    et lance le 'main' de chaque module avec les options
    respectives renseignées dans le dit fichier.
    """
    config = parse_config(config_file)
    setproctitle(os.path.basename(config_file).split('.')[-2].upper())
    max_workers = len(config) if max_workers is None else max_workers
    with ProcessPoolExecutor(max_workers=max_workers) as executor:
        future = {}
        for app in config:
            module, args = config[app]
            future[app] = executor.submit(call_main, app, module, args)
        while True:
            for app in future:
                print(app, future[app].running())
            sleep(1)


if __name__ == '__main__':
    parser = ArgumentParser(description=main.__doc__)
    parser.add_argument('config_file', help="Configuration file")
    parser.add_argument('--max_workers', '-m', type=int,
                        help="Nb maxi de process à lancer en //")
    args = parser.parse_args()
    main(**vars(args))
