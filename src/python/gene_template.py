#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- gene_template.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
"""
from argparse import ArgumentParser
from configparser import ConfigParser, ExtendedInterpolation
import os
import json

from SimpleSHM.redis_stream import __app_version
from SimpleSHM.utils import parse_strval

template = """#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- {file_name} ---
# Author  : {author_name}
# License : GPLv2
\"\"\"
Skeletal application
\"\"\"

from argparse import ArgumentParser

from SimpleSHM import RedisStream, shm_parser, Waiting, RedisHelpFormatter


parser = ArgumentParser(parents=[shm_parser],
                        formatter_class=RedisHelpFormatter,
                        description=__doc__, epilog='{epilog}')
{args_added}
def main({args}{comma}inputs={inputs}, outputs={outputs}, **shm_args):

    redis = RedisStream(**shm_args)

    ## To SHM
    periode = Waiting()
    for measure in gene_measure:
        redis.write_data(outputs[0], field1=measure.field1)
        periode.wait_next()

    ## From SHM
    out_device = Device()
    ### Cyclic work
    for data1, data2 in redis.watch_datas(*inputs, frequency=frequency):
        # do work
        out_device.write()

    ### Synced work
    for data in redis.watch_datas(*inputs):
        # do work
        out_device.write()


if __name__ == '__main__':
    args = {{k: v for k, v in vars(parser.parse_args()).items() if v is not None}}
    main(**args)
"""


def main(config_file, module_name):
    cfg = ConfigParser(interpolation=ExtendedInterpolation())
    cfg.read(config_file)
    options = cfg[module_name]
    print(dict(options))
    file_name = options['exec']
    inputs = options.get('inputs', '').split(' ')
    outputs = options.get('outputs', '').split(' ')
    for option in ('exec', 'inputs', 'outputs', 'hardware_in', 'hardware_out'):
        if option in options:
            del options[option]

    args_added = ''
    args = []
    for option in options:
        value = parse_strval(options[option])
        kind = value.__class__.__name__
        if kind == 'str':
            value = "'{}'".format(value)
        args_added += "parser.add_argument('--{}', type={}, help=' ',"\
                      "metavar='{}', default={})\n".\
            format(option, kind, kind, value)
        args.append('%s=%s' % (option, value))
    args = ', '.join(args)
    comma = ', ' if args else ''
    author_name = os.getenv('USER')
    epilog = __app_version
    open(file_name, 'w').write(template.format(**vars()))


if __name__ == '__main__':
    parser = ArgumentParser(description=main.__doc__)
    parser.add_argument('config_file', help="Configuration file")
    parser.add_argument('module_name')
    main(**vars(parser.parse_args()))
