#!/bin/sh
export RLWRAP_HOME=$HOME/.cache/rlwrap
mkdir -p "${RLWRAP_HOME}"
touch "${RLWRAP_HOME}"/shm_cli_history
cat > "${RLWRAP_HOME}"/commands_completion <<-EOF
ADD_DATA
WRITE_DATA
WRITE_DATAS
READ_DATA
READ_DATAS
DATA_RANGE
WATCH_DATA
WATCH_DATAS
WAIT_DATA
WAIT_DATAS
REMOVE_DATA
REMOVE_DATAS
LIST_DATA
DATA_TREE
ADD_ALIAS
INIT_DB
EOF
rlwrap -C 'shm_cli' -r -f "${RLWRAP_HOME}"/commands_completion -f . -A -p -S 'SHM> ' python3 -m SimpleSHM $@

