#!/usr/bin/env python

from setuptools import setup

setup(
    name='SimpleSHM',
    version='0.8',
    description='Networked Shared Memory based on Redis streams',
    author='Samuel Bucquet',
    author_email='samuel.bucquet@gmail.com',
    url='https://framagit.org/samgratte/redis_stream',
    packages=['SimpleSHM'],
    install_requires=['hiredis', 'psutil'],
    scripts=['shm_cli.sh', 'gene_dot.py', 'gene_template.py']
)
