WEBAPPDIR = brythonapp
WSDDEBUG = debug
# defaults to
# REDISHOST = localhost
# REDISPORT = 6379
REDISHOST = localhost
REDISPORT = 9999
WEBPORT = 8989
PYTHONVERSION = 3


.PHONY: redis webapp_srv gene_sin go

redis:
	xterm -T RedisServer -hold -e $(HOME)/work/redis/src/redis-server --port $(REDISPORT) --protected-mode no&

webapp_srv:
	xterm -T Websocketd -hold -geometry 1980x700+0+0\
	   	-e websocketd --staticdir $(WEBAPPDIR) --port=$(WEBPORT)\
	   	--loglevel=$(WSDDEBUG)\
	      	python$(PYTHONVERSION) -u -m SimpleSHM -H $(REDISHOST) -P $(REDISPORT) -j -X 'WS' -d -D &

gene_sin:
	xterm -T GeneSinusoids -hold -e python$(PYTHONVERSION) -u examples/gene_sinusoids.py -a -H $(REDISHOST) -P $(REDISPORT) &

go: 
	xdg-open http://localhost:$(WEBPORT)/redis_interface.html&

install:
	cd src/python ;\
	python$(PYTHONVERSION) setup.py develop --user
