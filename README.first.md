---
#title: --- à modifier ---
#author: Samuel Bucquet
date:
dedication:
resume:
documentclass: scrartcl
classoption:
# - twoside
# - chapterprefix
# - headings=big
# - appendixprefix
# - chapterentrydots
# - sectionentrydots
# - headsepline
# - footsepline
 - openany
 - oneside
# - landscape
# - twocolumn
fontfamily: libertine
fontsize: 10pt
lang: en
papersize: a4
geometry:
 - vmargin=20mm
 - hmargin=10mm
numbersections: false
links-as-notes: true
toc: false
header-includes:
 - \usepackage{lipsum}
### changement des labels de liste par des triangles
 - \usepackage{enumitem}
 - \usepackage{amsfonts}
 - \usepackage{fancyvrb}
 - \setlist[itemize,1]{label=$\blacktriangleright$}
### Sélection d'une taille plus petite pour les listings et verbatim
 - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\},fontsize=\scriptsize}
 - \usepackage{etoolbox}
 - \makeatletter
 - \patchcmd{\@verbatim}{\verbatim@font}{\verbatim@font\footnotesize}{}{}
 - \makeatother
---

# SimpleSHM dependencies & installation

## Core components

```
sudo apt-get install git python3-setuptools python3-dev python3-pip rlwrap
```
```
cd work
git clone git@framagit.org:samgratte/redis_stream.git SimpleSHM
# or on local network
git clone gitserver:redis_stream SimpleSHM
cd SimpleSHM
make install
```

## Optionnal

```
sudo apt-get install xterm python3-graphviz xdot
```

## WEB clients

### websocketd

```
wget https://github.com/joewalnes/websocketd/releases/download/v0.3.1/websocketd-0.3.1_amd64.deb
sudo dpkg -i websocketd-0.3.1_amd64.deb
```

### Brython
```
pip3 install brython --user
cd ~/work
mkdir brythonjs
cd brythonjs
python3 -m brython --install  # or --update
cd $WEBAPP_DIR
ln -s ~/work/brythonjs
```

# Work session

## Redis

Choose a host and launch redis on it.
If this is a one time instance, you can launch it with `make redis`.

## example app

```
make gene_sin
```

## Web app server

```
make webapp_srv
```

## Web client

```
make go
```
