#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- hdl_gpio_out.py ---
# Author  : samuel
# License : GPLv2
"""
Skeletal application
"""

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

# from divers import get_git_branch
from redis_stream import RedisStream, shm_parser


parser = ArgumentParser(parents=[shm_parser],
                        formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__, epilog=get_git_branch())
parser.add_argument('--gpio_num', type=int, help=' ',metavar='int', default=60)

def main(gpio_num=60, inputs=['THRUSTERS__Batt_discharge_cmd'], outputs=[''], **shm_args):

    redis = RedisStream(**shm_args)

    ## To SHM
    periode = Waiting()
    for measure in gene_measure:
        redis.write_data(outputs[0], field1 = measure.field1)
        periode.wait_next()

    ## From SHM
    out_device = Device()
    ### Cyclic work
    periode = Waiting()
    while True:
        shm_datas = redis.read_datas(*inputs)
        # do work
        out_device.write()
        periode.wait_next()

    ### Synced work
    for shm_datas in redis.watch_datas(*outputs):
        # do work
        out_device.write()


if __name__ == '__main__':
    args = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}
    main(**args)
