#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- gene_template.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
"""
from argparse import ArgumentParser
from configparser import ConfigParser, ExtendedInterpolation
import os
import json

from divers import parse_strval

template = """#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- {file_name} ---
# Author  : {author_name}
# License : GPLv2
\"\"\"
Skeletal application
\"\"\"

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from divers import get_git_branch
from shareddata import SharedMemory, shm_parser


parser = ArgumentParser(parents=[shm_parser],
                        formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__, epilog=get_git_branch())
{args_added}
def main({args}{comma}inputs={inputs}, outputs={outputs}, **shm_args):

    with SharedMemory(**shm_args) as shm:

        def print_trame(trame):
            print('%s received %s : %s' % (shm_args['app_name'],
                                           trame.name, trame))
        shm_read = shm.read if 'frequency' in vars() else shm.watch
        ### only inputs ###
        if len(inputs) > 1:
            for trames in shm_read(inputs):
                for trame in trames:
                    print_trame(trame)
        elif len(inputs) == 1:
            for trame in shm_read(inputs[0]):
                print_trame(trame)
        else:
            ### only outputs ###
            shm_data = shm.get_data_hdlr(outputs[0], validity=10, priority=99)
            while True:
                # reading sensor for ex
                shm_data.write(field1=result1, field2=result2)

        ### inputs and outputs ##

        shm_data = shm.get_data_hdlr(outputs[0], validity=10, priority=99)
        for trame1, trame2 in shm_read(inputs):
            # trame1 & trame2 refreshed from shm at every loop
            # doing some work ...
            shm_data.write(field1=result1, field2=result2)
            # wait for next iteration ... (with divers.Waiting() for ex.)

if __name__ == '__main__':
    main(**vars(parser.parse_args()))
"""


def main(config_file, module_name):
    cfg = ConfigParser(interpolation=ExtendedInterpolation())
    cfg.read(config_file)
    options = cfg[module_name]
    print(dict(options))
    file_name = options['exec']
    inputs = options.get('inputs', '').split(' ')
    outputs = options.get('outputs', '').split(' ')
    for option in ('exec', 'inputs', 'outputs', 'hardware_in', 'hardware_out'):
        if option in options:
            del options[option]

    args_added = ''
    args = []
    for option in options:
        value = parse_strval(options[option])
        kind = value.__class__.__name__
        if kind == 'str':
            value = "'{}'".format(value)
        args_added += "parser.add_argument('--{}', type={}, help=' ',"\
                      "metavar='{}', default={})\n".\
            format(option, kind, kind, value)
        args.append('%s=%s' % (option, value))
    args = ', '.join(args)
    comma = ', ' if args else ''
    author_name = os.getenv('USER')
    open(file_name, 'w').write(template.format(**vars()))


if __name__ == '__main__':
    parser = ArgumentParser(description=main.__doc__)
    parser.add_argument('config_file', help="Configuration file")
    parser.add_argument('module_name')
    main(**vars(parser.parse_args()))
