#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- launcher.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
"""

from argparse import ArgumentParser
from configparser import ConfigParser, ExtendedInterpolation
import pygraphviz


def main(dotfile, longname, config_files, ranksep=0.2, splines='splines',
         orientation='TB'):
    cfg = ConfigParser(interpolation=ExtendedInterpolation())
    cfg.read(config_files)
    cfg.remove_section('SHM')  # if present
    hw_nodes = []
    datas_nodes = []
    mods_nodes = []
    config_nodes = {}
    hostname = 'localhost'

    G = pygraphviz.AGraph(directed=True, splines=splines, rankdir=orientation,
                          ranksep=ranksep, bgcolor="lightgrey")
    G.graph_attr.update(label=longname, labelloc="t", labeljust="r")
    G.node_attr.update(shape="box", fillcolor="white", style="filled")
    G.edge_attr.update(arrowhead="open")

    for app in cfg.sections():
        options = cfg[app]
        if 'exec' not in options:
            continue
        config_nodes[app] = {}
        for k, v in options.items():
            print(k, v)
            if v == '':
                continue
            if k in ('hardware_in', 'hardware_out'):
                # HardWare
                hw_nodes.append(v)
                G.add_node(v, color="red", style="rounded,filled")
            elif k in ('inputs', 'outputs'):
                # Datas
                if ' ' in v:
                    datas_nodes.extend(v.split(' '))
                else:
                    datas_nodes.append(v)
            elif k == 'host':
                hostname = v
            elif k == 'exec':
                # Modules
                module = v
            else:
                # Configs
                config_nodes[app][k] = v
        G.add_node(app, label='%s\non %s\n(%s)' % (app, hostname, module),
                   fillcolor="yellow", style="rounded,filled")

    datas_nodes = set(datas_nodes)  # remove duplicates
    G.add_nodes_from(datas_nodes, color="blue")
    for n in G.nodes():
        if 'ALARMS__' in n:
            n.attr['color'] = 'orange'
    for c in config_nodes:
        if not config_nodes[c]:
            continue
        label = '{Initial params'
        for k, v in config_nodes[c].items():
            label += '|{%s|%s}' % (k, v)
        label += '}'
        G.add_node('conf_'+c, label=label, shape="record")
        G.add_edge('conf_'+c, c)

    for app in cfg.sections():
        options = cfg[app]
        if 'exec' not in options:
            continue
        for k in options:
            if k in ('inputs', 'hardware_in'):
                if options[k] in datas_nodes.union(set(hw_nodes)):
                    # options[k] is scalar
                    G.add_edge(options[k], app)
                else:
                    for n in datas_nodes:
                        if n in options[k]:
                            G.add_edge(n, app)
            elif k in ('outputs', 'hardware_out'):
                if options[k] in datas_nodes.union(set(hw_nodes)):
                    # options[k] is scalar
                    G.add_edge(app, options[k])
                else:
                    for n in datas_nodes:
                        if n in options[k]:
                            G.add_edge(app, n)

    G.write(dotfile)


if __name__ == '__main__':
    parser = ArgumentParser(description=main.__doc__)
    parser.add_argument('dotfile', help="graphviz dot file written")
    parser.add_argument('longname', help="Application description")
    parser.add_argument('config_files', metavar="launcher_<host>.ini",
                        type=str, nargs='+', help="Configuration files")
    parser.add_argument('-s', '--splines', default="splines",
                        help="spacing between nodes")
    parser.add_argument('-o', '--orientation', default='TB',
                        help="spacing between nodes")
    parser.add_argument('-r', '--ranksep', type=float, default=0.2,
                        help="spacing between nodes")
    args = parser.parse_args()
    main(**vars(args))
