"""
Brython module for interfacing with webdis, a HHTP access to Redis.

offers <Webdis> and <SHMView>

-*- coding: utf-8 -*-
--- brytredis.py ---
Author  : samuel.bucquet@gmail.com
License : GPLv2
"""


import json
from collections import namedtuple
from browser import ajax, websocket
from browser.session_storage import storage
from browser.object_storage import ObjectStorage
from browser.timer import set_timeout

object_storage = ObjectStorage(storage)


def parse_strval(v):
    """Renvoie un scalaire python à partir d'une chaîne."""
    v = v.strip()
    if len(v) == 0:
        return v
    if v[0] in ('"', "'"):
        v = v[1:-1]
    if v.upper() in ('TRUE', 'YES', 'ON'):
        return True
    elif v.upper() in ('FALSE', 'NO', 'OFF'):
        return False
    else:
        try:
            return int(v)
        except (ValueError, TypeError):
            pass
        try:
            return float(v)
        except (ValueError, TypeError):
            pass
        return v


def getnamedtuple(dataname, dico):
    """Return a namedtuple from the dict argument."""
    #dico = {k: parse_strval(v) for k, v in dico.items()}
    dico = {k: v for k, v in dico.items()}
    nt = namedtuple(dataname, sorted(dico.keys()))
    return nt._make([dico[k] for k in sorted(dico.keys())])


class Webdis(object):
    """Mask the HTTP protocol used for the Redis access."""

    def __init__(self, base_url):
        """Init with <base_url>."""
        self.base_url = base_url
        self.websockets = {}

    def __enter__(self):
        """Enter context."""
        return self

    def __exit__(self, exctype, val, tb):
        """Exit context."""
        if len(self.websockets):
            for ws in self.websockets:
                self.websockets[ws].close()

    def read_data(self, dataname, callback, *fields):
        """<callback> is called with a namedtuple filtered by <fields>."""
        request = ajax.ajax()
        url = 'http://{}SHM?dataname={}'.format(self.base_url, dataname)
        if len(fields):
            for f in fields:
                url += '&fields=' + f

        def on_complete(req):
            if req.status == 404:
                print(req.text)
                callback(None)
            else:
                print(req.text)
                callback(getnamedtuple(dataname, json.loads(req.text)))

        request.open('GET', url, True)
        request.bind('complete', on_complete)
        request.send()

    def write_data(self, dataname, **kvargs):
        """Send the new values to Redis via HTTP."""
        request = ajax.ajax()
        params = kvargs
        params['dataname'] = dataname
        request.open('POST', 'http://{}/SHM'.format(self.base_url), True)
        request.open('POST', 'http://'+self.base_url, True)
        request.send(json.dumps(params))

    def watch_data(self, dataname, callback, frequency=-1, *fields):
        """<callback> is only called if dataname is refreshed."""
        url = 'ws://{}/SHM_watch/{}'.format(self.base_url, dataname,
                                            '/{}'.format(frequency) if frequency > 0 else '')
        ws = websocket.WebSocket(url)

        def parse_data(evt):
            callback(getnamedtuple(dataname, json.loads(evt.data)))

        ws.bind('message', parse_data)
        return ws

    """
    # maybe it's worth implementing something ...
    def watch_bin_data(self, dataname, callback):
        url = 'ws://' + self.base_url + '/{}'.format(dataname)
        ws = websocket.WebSocket(url)
        ws.binaryType = "arraybuffer"

        def parse_bin_data(evt):
            uint8array = window.Uint8Array
            bufview = uint8array.new(evt.data)
            print(bufview)
            print(evt.data.byteLength)
            print(bufview[1])

            # callback(getnamedtuple(dataname, json.loads(evt.data)))

        ws.bind('message', parse_bin_data)
    """


class SHM_view(dict):
    """Offers a view on a selection of Redis datas cached locally."""

    def __init__(self, base_url, frequency, viewid=None, *datanames, **datas):
        """From where (base_url) and how often (frequency) do we cache."""
        super().__init__(self)
        if len(datas):
            self.fieldsubs = datas
        elif len(datanames):
            self.namesubs = datanames
        self.base_url = base_url
        self.viewid = viewid
        self.frequency = frequency
        self._prepare_url()
        self._connect_ws()

    def _prepare_url(self):
        url = 'ws://{}/SHM_view/{}?'.format(self.base_url, self.frequency)
        for name in self.namesubs:
            url += '&dataname={}'.format(name)
        for name, fields in self.fieldsubs.items():
            url += '&dataname={}'.format(name)
            for field in fields:
                url += '&{}_fields={}'.format(name, field)
        if self.viewid:
            url += '&viewid={}'.format(self.viewid)
        self.url = url

    def _connect_ws(self):
        self.ws = websocket.WebSocket(self.url)
        self.ws.bind('message', self._receive_viewid)
        self.ws.bind('error', self._ws_error)

    def _ws_error(self):
        # try to reconnect every 2s:
        set_timeout(self._connect_ws, 2000)

    def _receive_viewid(self, evt):
        self.viewid = evt.data.split(':').strip()
        self.ws.bind('message', self._receive_datas)

    def _receive_datas(self, evt):
        object_storage[self.viewid] = json.loads(evt.data)

    def __getitem__(self, dataname):
        """Retrieve the cached value for <dataname> in a namedtuple."""
        return getnamedtuple(dataname, object_storage[self.viewid][dataname])

    def __setitem__(self, dataname, fields):
        """Ask for the data <dataname> with <fields> (fields='All' for all)."""
        request = ajax.ajax()
        url = 'http://{}/SHM_view/{}/{}'.format(
                 self.base_url, self.viewid, dataname)
        request.open('PUT', url, True)
        request.send(json.dumps(dict(fields=fields)))

    def __delitem__(self, dataname):
        """Don't want <dataname> anymore."""
        request = ajax.ajax()
        url = 'http://{}/SHM_view/{}/{}'.format(
                 self.base_url, self.viewid, dataname)
        request.open('DELETE', url, True)
        request.send('unsubscribe')

    def __contains__(self, dataname):
        """Do we cache <dataname>."""
        return dataname in object_storage[self.viewid]

    def __iter__(self):
        """Allow iteration on this view."""
        return object_storage[self.viewid]

    def __len__(self):
        """Return the number of data subscribed and cached."""
        return len(object_storage[self.viewid])
