#!/usr/bin/python3
# -*- coding: utf-8 -*-
# --- redis_interface.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
In order to acces this files you have to serve them with an HTTP server.
The dependencies are the Brython javascript files:
    brython.js & brython_stdlib.js or brython_modules.js

you install them in the static directory of your HTTP server like this:
    $ pip3 install brython
    $ cd static/
    $ python3 -m brython --install
    $ python3 -m brython --modules
and you serve them like this for example:
    $ python -m brython --server --port 8081
"""

import json
from collections import OrderedDict, namedtuple, defaultdict
from datetime import datetime
# from functools import partialmethod

from browser import alert, document, html, window, svg
from browser import websocket, timer
from browser.html import TABLE, TR, TD, B, I, SVG


class WebSocket:

    def __init__(self, receive_cb, base_url=None):
        self.base_url = base_url if base_url else 'ws://{}/'.format(
            document.location.host)
        self.connected = False
        self.receive_cb = receive_cb
        self.connect()
        self.data_binds = {}

    def connect(self):
        print('trying connecting to {} ...'.format(self.base_url))
        self.whenconnect = datetime.now()
        self.ws = websocket.WebSocket(self.base_url)
        self.ws.bind('open', self._open)
        self.ws.bind('error', self.error)

    def _open(self, evt):
        print('connected', datetime.now() - self.whenconnect)
        self.connected = True
        evt.target.bind('close', self.close)
        evt.target.bind('message', self.receive_cb)

    def close(self):
        print('disconnected')
        self.connected = False

    def error(self):
        alert("Websocket Error !")

    def send_cmd_args(self, cmd, *args):
        self.ws.send('{} {}\r\n'.format(cmd, ' '.join(args)))

    def data_bind(self, dataname, callback, component, *fieldnames):
        self.data_binds[dataname] = (callback, component) + fieldnames

    def data_unbind(self, *datanames):
        for dataname in datanames:
            del self.data_binds[dataname]


class Webdis(WebSocket):

    def __init__(self, base_url=None, do_when_ready=None):
        super().__init__(self.receive_datas, base_url=base_url)
        self.websockets = []
        self.data_handling = True
        self.do_when_ready = do_when_ready

    def _open(self, evt):
        super()._open(evt)
        if self.do_when_ready:
            self.do_when_ready(self)

    def watch_data(self, *datanames, frequency=1):
        watch_ws = websocket.WebSocket(self.base_url)

        def init_watch(evt):
            WebSocket._open(self, evt)
            evt.target.send('WATCH_DATAS {} {}\r\n'.format(frequency, ' '.join(datanames)))
            print('watch initiated on ', *datanames)

        watch_ws.bind('open', init_watch)

    def wait_data(self, *datanames, timeout=0):
        wait_ws = websocket.WebSocket(self.base_url)

        def init_watch(evt):
            WebSocket._open(self, evt)
            evt.target.send('WAIT_DATAS {} {}\r\n'.format(timeout, ' '.join(datanames)))
            print('wait initiated on ', *datanames)

        wait_ws.bind('open', init_watch)

    def data_range(self, dataname, first, last):
        self.send_cmd_args('DATA_RANGE', dataname, first, last)

    def read_data(self, *datanames):
        self.send_cmd_args('READ_DATA', *datanames)

    def write_data(self, dataname, add_mode=False, **kvargs):
        self.send_cmd_args('ADD_DATA' if add_mode else 'WRITE_DATA',
                           dataname, *(it for tu in kvargs.items() for it in tu))

    def receive_datas(self, evt):
        if evt.data[0] == '[':
            # datas = json.loads(evt.data, object_pairs_hook=OrderedDict)
            datas = json.loads(evt.data)
            for data in datas:
                dataname = data['name']
                update_debug_table(data)
                if not self.data_handling:
                    continue
                for component, action, label in update_actions[dataname]:
                    action(component, data, label)
        else:
            alert(evt.data)


if __name__ == '__main__':

    def once_ready(event):
        datanames = document["input:readdata"].value.split()
        for dataname in datanames:
            if event.target.text == 'READ':
                redis.read_data(*datanames)
            elif event.target.text == 'WATCH':
                redis.watch_data(*datanames)

    def writedata(event):
        dataname, *args = document["input:writedata"].value.split()
        it = iter(args)
        redis.write_data(dataname, add_mode=event.target.text == 'ADD',
                         **OrderedDict((k, v) for k, v in zip(it, it)))

    def stopdata(event):
        redis.data_handling = False

    def resumedata(event):
        redis.data_handling = True

    document["readbtn"].bind("click", once_ready)
    document["watchbtn"].bind("click", once_ready)
    document["writebtn"].bind("click", writedata)
    document["addbtn"].bind("click", writedata)
    document["stopbtn"].bind("click", stopdata)
    document["resumebtn"].bind("click", resumedata)

    print("Zorglub")
    update_actions = defaultdict(list)
    print("Zorglub")

    def mkId(*elts):
        return '_'.join(elts)

    def send_edited_data(evt):
        row = evt.target.parentNode.parentNode
        redis.write_data(row.id, **{elt.dataset.field: elt.value for elt in row.select('input')})
        toggle_dataedit(row.select('td')[0])

    def toggle_dataedit(elt):
        if elt.parentNode.dataset.edit == "no":
            elt.style.background = "red"
            elt.parentNode.dataset.edit = "yes"
            for e in elt.parentNode.select('label'):
                value, field = e.text, e.dataset.field
                cell = e.parentNode
                e.remove()
                cell <= html.INPUT(value=value, data_field=field)
            btn = html.BUTTON('SEND', id='btn:'+elt.id)
            btn.bind('click', send_edited_data)
            elt.parentNode <= TD(btn)
        else:
            elt.style.background = "lightgrey"
            elt.parentNode.dataset.edit = "no"
            for e in elt.parentNode.select('input'):
                value, field = e.value, e.dataset.field
                cell = e.parentNode
                e.remove()
                cell <= html.LABEL(value, data_field=field)
            document['btn:'+elt.id].parentNode.remove()

    def debug_dataedit(evt):
        toggle_dataedit(evt.target)

    def update_debug_table(datadict):
        if document['debug'].style.display == 'none':
            return
        table = document["datadebug"]
        dataname = datadict['name']
        row = document.select('#'+dataname)
        if len(row) > 0:
            row = row[0]
        else:
            row = TR(id=dataname, data_edit="no")
            for key in datadict:
                cell = TD(id=mkId('cell', dataname, key), align='center')
                if key == 'timestamp':
                    cell.bind('click', debug_dataedit)
                elif key == 'name':
                    cell <= html.B(dataname)
                row <= cell
            table <= row
        if row.dataset.edit == "yes":
            return
        for key in datadict:
            if key not in {'timestamp', 'name', 'origin', 'validity', 'priority', 'maxlen'}:
                cells = document.select('#'+mkId('cell', dataname, key))
                if cells:
                    cells[0].remove()
                    cell = TD(key+':'+html.LABEL(datadict[key], Class="datavalues",
                                                 data_field=key),
                              align='left', id=mkId('cell', dataname, key))
                row <= cell
            else:
                cell = document[mkId('cell', dataname, key)]
                if key == 'timestamp':
                    cell.text = datetime.utcfromtimestamp(datadict[key]).strftime('%H:%M:%S.%f')
                elif key == 'name':
                    continue
                else:
                    cell.clear()
                    cell <= html.LABEL(str(datadict[key]), data_field=key)

    def update_gauge(gaugeid, datadict, label):
        gauge = document.gauges.get(gaugeid)
        gauge.value = float(datadict[label])

    def update_timeserie(timeserieid, datadict, label):
        timeserie = document.timeseries[datadict['name']+'_'+label]
        timeserie.append(datadict['timestamp']*1000, datadict[label])

    def update_statuslight(statuslightid, datadict, label):
        id = statuslightid + ':' + datadict['name'] + ':' + label
        print(repr(datadict))
        if not datadict['validity']:
            color = 'lightgrey'
        elif 'Toggle' == datadict[label]:
            print('Toggle')
            color = 'green' if document[id].style.fill == 'red' else 'red'
        else:
            color = 'green' if datadict[label] else 'red'
        document[id].style.fill = color

    def refresh_redis_conx():
        if redis.connected:
            document["conx"].style.backgroundColor = "green"
        else:
            document["conx"].style.backgroundColor = "red"
            # redis.connect()
        timer.set_timeout(refresh_redis_conx, 1000)

    update_fn = {
        'timeserie': update_timeserie,
        'gauge': update_gauge,
        'status-light': update_statuslight
    }
    datas_to_watch, datas_to_wait = set(), set()
    TimeSeries = window.TimeSeries
    document.timeseries = {}
    colors = ('rgba(0, 255, 0, 1)', 'rgba(255, 0, 0, 1)', 'rgba(0, 0, 255, 1)')
    table = document["datadebug"]
    for elt in document.select('[data-shm-read]'):
        fields = elt.dataset.shmFields.split(',')
        dataname = elt.dataset.shmName
        print(dataname, fields)
        if elt.dataset.shmAnchor == "timeserie":
            # SmoothieCharts
            SmoothieChart = window.SmoothieChart
            chart = SmoothieChart.new({
                'millisPerPixel': 200,
                'scrollBackwards': False,
                'tooltip': True,
                'limitFPS': 10,
                'interpolation': 'line',
                'timestampFormatter': SmoothieChart.timeFormatter,
                'grid': {'millisPerLine': 5000, 'verticalSections': 0}})
            for label, color in zip(fields, colors):
                ts_id = dataname + '_' + label
                if ts_id not in document.timeseries:
                    document.timeseries[ts_id] = TimeSeries.new()
                chart.addTimeSeries(document.timeseries[ts_id],
                                    {'strokeStyle': color, 'lineWidth': 1})
            chart.streamTo(elt, 1000)
        elif elt.dataset.shmAnchor == 'status-light':
            id = elt.id + ':' + dataname + ':' + fields[0]
            w, h = int(elt.width)/2, int(elt.height)
            elt <= svg.circle(id=id, cx=w, cy=w, r=w-5, stroke='black',
                              stroke_width='2', fill='gray')
            elt <= svg.text(fields[0], x=w, y=h-3, font_size=16,
                            text_anchor='middle')
        for field in fields:
            update_actions[dataname].append(
                (elt.id, update_fn[elt.dataset.shmAnchor], field))
        if elt.dataset.shmRead == "watch":
            datas_to_watch.add(dataname)
        elif elt.dataset.shmRead == "wait":
            datas_to_wait.add(dataname)

    def send_value_via_buttonclick(ev):
        redis.write_data(ev.target.dataset.shmName,
                         **dict(zip(ev.target.dataset.shmFields.split(','),
                                    ev.target.dataset.shmValues.split(','))))

    def send_choosen_action(ev):
        # FIXME: the first elt returned via document.select is void
        elt = document.select('input:checked', ev.target.dataset.parentid)[1]
        dataname, field, value = elt.id.split(':')
        redis.write_data(dataname, **dict(((field, value),)))

    for elt in document.select('[data-shm-write]'):
        fields = elt.dataset.shmFields.split(',')
        values = elt.dataset.shmValues.split(',')
        dataname = elt.dataset.shmName
        if elt.dataset.shmAnchor == "write-value":
            elt.bind("click", send_value_via_buttonclick)
        elif elt.dataset.shmAnchor == "choose-value":
            for value in values:
                id = dataname+':'+fields[0]+':'+value
                elt <= html.INPUT(id=id, type="radio", name="dataname", value=value)
                elt <= html.LABEL(value, For=id)
            document[id].checked = True
            btn = html.BUTTON('ENVOYER', data_parentid=elt.id)
            btn.bind('click', send_choosen_action)
            elt <= btn

    def when_connected(cnx):
        cnx.read_data(*datas_to_watch.union(datas_to_wait))
        cnx.watch_data(*datas_to_watch)
        for dataname in datas_to_wait:
            cnx.wait_data(dataname)

    redis = Webdis(do_when_ready=when_connected)

    timer.set_timeout(refresh_redis_conx, 1000)
