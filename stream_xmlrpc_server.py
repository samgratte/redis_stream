#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# --- stream_xmlrpc_server.py ---
# Author  : samuel.bucquet@gmail.com
# License : GPLv2
"""
XML-RPC interface to Redis streams for web clients and GUI
"""


import sys
from xmlrpc.server import DocXMLRPCServer
from xmlrpc.server import DocXMLRPCRequestHandler
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from redis_stream import RedisStream, shm_parser

parser = ArgumentParser(parents=[shm_parser], formatter_class=ArgumentDefaultsHelpFormatter,
                        description=__doc__)
parser.add_argument('-f', '--frequency', type=float, default=1,
                    help='frequency to send the false signals')
parser.add_argument('-a', '--add_data', action='store_true',
                    help='Init data before sending')

def main(tcpport=8888, inputs=[], outputs=[], **shm_args):

    redis = RedisStream(**shm_args)

if __name__ == '__main__':
    args = parser.parse_args()
    main(**vars(args))
